<?php

use SCAAI\Kernel\Analytics;
use SCAAI\Kernel\Normalizer;

class Importar
{
    private $analytics;

    public function __construct()
    {
        $this->analytics = new Analytics();
    }

    public function index()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (!empty($_FILES)) {
                $dataSet   = file_get_contents($_FILES['file']['tmp_name']);
                $data_norm = new Normalizer($dataSet);
                $send      = $this->constructDataSend($data_norm->getAllArray());
                return $this->analytics->getSentiment($send)->executeAnalysis();
                //return $send;
            } else {
                return 'Envie un dataset';
            }
        } else {
            return 'Petición Invalida';
        }
    }

    private function constructDataSend(array $data_norm_array)
    {
        try {
            $ret_arr['documents'] = array();
            $inc                  = 1;
            foreach ($data_norm_array as $type) {
                foreach ($type as $item) {
                    array_push($ret_arr['documents'], [
                        'id'   => $inc++,
                        'text' => $item
                    ]);
                }
            }
            return $ret_arr;
        } catch (Throwable $th) {
            die('ERROR_CONSTRUCT_DATA_SEND: ' . $th->getMessage());
        }
    }
}