<?php

namespace SCAAI\Kernel;
require 'Inflector.php';
require 'Analytics.php';
require 'Normalizer.php';

use Throwable;
use SCAAI\Kernel\Inflector;

class Request
{
    protected $controller;
    protected $default_controller = 'Home';
    protected $method;
    protected $default_method     = 'index';
    protected $parameters;
    private   $url;

    public function __construct($url)
    {
        $this->url = $url;
        //Construct url segments glue = /
        $segments = explode('/', $this->getUrl());

        //Construct Class->methods
        $this->setController($segments)->setMethod($segments)->setParameters($segments);
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    protected function setParameters(&$parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

    protected function setMethod(&$method)
    {
        $this->method = array_shift($method);
        $this->method = (empty($this->method)) ? $this->default_method : htmlentities(addslashes($this->method));
        return $this;
    }

    protected function setController(&$controller)
    {
        $this->controller = array_shift($controller);
        $this->controller = (empty($this->controller)) ? $this->default_controller : htmlentities(addslashes($this->controller));
        return $this;
    }

    public function execute()
    {
        try {
            extract($this->getClassObject());

            /** @var string $controllerFileName */
            if (file_exists($controllerFileName)) {
                require $controllerFileName;

                /** @var object $controllerClassName */
                $controller = new $controllerClassName();
                /** @var string $methodName */
                /** @var array $params */
                $response = call_user_func_array([$controller, $methodName], [$params]);

                $this->executeResponse($response);
            }
        } catch (Throwable $th) {
            die('ERROR_EXECUTE_RESPONSE: ' . $th->getMessage());
        }
    }

    protected function getClassObject()
    {
        try {
            $controllerClassName = $this->getControllerClassName();
            $controllerFileName  = $this->getControllerFileName();
            $methodName          = $this->getMethodName();
            $params              = $this->getParams();

            return compact('controllerClassName', 'controllerFileName', 'methodName', 'params');
        } catch (Throwable $th) {
            die('ERROR_GET_OBJECT_CLASS: ' . $th->getMessage());
        }
    }

    private function getControllerClassName(): string
    {
        return Inflector::upperCamelCase($this->controller);
    }

    private function getControllerFileName()
    {
        try {
            //Resolve Controllers PATH
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/app/Controllers/API/' . $this->getControllerClassName() . '.php')) {
                return $_SERVER['DOCUMENT_ROOT'] . '/app/Controllers/API/' . $this->getControllerClassName() . '.php';
            }
            return false;
        } catch (Throwable $th) {
            die('ERROR_GET_CONTROLLER_FILENAME: ' . $th->getMessage());
        }
    }

    private function getMethodName(): string
    {
        return Inflector::lowerCamelCase($this->method);
    }

    private function getParams()
    {
        return $this->parameters;
    }

    protected function executeResponse(&$response)
    {
        try {
            if ($response instanceof Response) {
                $response->execute();
            } elseif (is_array($response)) {
                http_response_code(202);
                echo json_encode($response, JSON_PRETTY_PRINT);
            } elseif (is_string($response)) {
                http_response_code(500);
                header('Error: ' . $response);
                echo $response;
            } elseif (is_null($response)) {
                http_response_code(404);
                //header('Pagina no encontrada :(');
            }
        } catch (Throwable $th) {
            die('ERROR_EXECUTE_RESPONSE: ' . $th->getMessage());
        }
    }
}
