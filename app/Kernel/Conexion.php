<?php
require 'config_local.php';

class Conexion extends Config
{
    private $pdo;

    public function __construct()
    {
        $this->pdo = new PDO("mysql:host=" . self::DB_HOST . ";dbname=" . self::DB_NAME, self::DB_USERNAME, self::DB_PASSWORD, self::DB_OPT);
        $this->pdo->exec(self::DB_CHAR);
    }

    public function conectDB($sql)
    {
        try {
            $this->pdo->commit();
            $pdoStatement = $this->pdo->prepare($sql);
            return [$pdoStatement, $this->pdo];
        } catch (Throwable $th) {
            $this->pdo->rollBack();
            die('ERROR_CON_DB: ' . $th->getMessage());
        }
    }

    public function getLastInsertId()
    {
        return $this->pdo->lastInsertId();
    }
}
