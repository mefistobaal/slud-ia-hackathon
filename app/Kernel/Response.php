<?php

namespace SCAAI\Kernel;
abstract class Response
{
    abstract public function execute();
}