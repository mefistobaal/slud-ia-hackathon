<?php

namespace SCAAI\Kernel;

class Normalizer

{
    private $fileContent;
    private $arrayNegativos = array();
    private $arrayPositivos = array();
    private $arrayNeutros   = array();

    public function __construct(string $fileContent)
    {
        $this->fileContent = $fileContent;
        $this->dataNormalize();
    }

    private function dataNormalize()

    {
        if (strpos($this->fileContent, '|Negativo') ||
            strpos($this->fileContent, '|Positivo') || strpos($this->fileContent, '|Neutral')) {
            $tmp = explode('|', $this->fileContent);
            for ($i = 0; $i < count($tmp); $i++) {
                if (strpos("'" . $tmp[$i] . "'", 'Negativo')) {
                    array_push($this->arrayNegativos, $tmp[$i]);
                }
                if (strpos("'" . $tmp[$i] . "'", 'Positivo')) {
                    array_push($this->arrayPositivos, $tmp[$i]);
                }
                if (strpos("'" . $tmp[$i] . "'", 'Neutral')) {
                    array_push($this->arrayNeutros, $tmp[$i]);
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getArrayNegativos(): array
    {
        return $this->arrayNegativos;
    }

    /**
     * @return array
     */
    public function getArrayPositivos(): array
    {
        return $this->arrayPositivos;
    }

    /**
     * @return array
     */
    public function getArrayNeutros(): array
    {
        return $this->arrayNeutros;
    }

    public function getAllArray()
    {
        return [$this->arrayPositivos, $this->arrayNegativos, $this->arrayNeutros];
    }
}