<?php


use SCAAI\Kernel\Analytics;

class Home
{
    private $analysis;

    public function __construct()
    {
        $this->analysis = new Analytics();
    }

    public function index()
    {
        return ['Algun mensaje del controlador'];
    }

    public function consultar()
    {
        return $this->analysis->getSentiment(['Hola como estas?', 'Me siento enfermo'])->executeAnalysis();
    }
}