<?php


namespace SCAAI\Kernel;
require 'config_local.php';

use Config;
use Exception;
use Throwable;

class Analytics extends Config
{
    private $api_key;
    private $api_endpoint;
    private $path;
    private $dataUTF8 = array();
    private $headers;
    private $options;
    private $streamContext;

    public function __construct()
    {
        $this->api_key      = self::KEY_SUB;
        $this->api_endpoint = self::KEY_ENDPOINT;
        $this->path         = $this->api_endpoint . '/sentiment';
    }

    public function getSentiment(array $data)
    {
        $this->constructData($data)->constructHeaders()->constructOptions();
        return $this;
    }

    private function constructOptions()
    {
        try {
            $this->options       = array(
                'http' => array(
                    'method'  => 'POST',
                    'header'  => $this->headers,
                    'content' => json_encode($this->dataUTF8)
                )
            );
            $this->streamContext = stream_context_create($this->options);
        } catch (Throwable $th) {
            die('ERROR_CONSTRUCT_OPTIONS: ' . $th->getMessage());
        }
    }

    private function constructHeaders()
    {
        try {
            $data          = json_encode($this->dataUTF8);
            $this->headers = "Content-type: text/json\r\n" .
                "Content-Length: " . strlen($data) . "\r\n" .
                "Ocp-Apim-Subscription-Key:" . $this->api_key . "\r\n";
            return $this;
        } catch (Throwable $th) {
            die('ERROR_CONSTRUCT_HEADERS: ' . $th->getMessage());
        }
    }

    private function constructData($data)
    {
        try {
            $this->dataUTF8 = $data;
            return $this;
        } catch (Throwable $th) {
            die('ERROR_CONSTRUCT_DATA: ' . $th->getMessage());
        }
    }

    public function executeAnalysis()
    {
        try {
            $result = file_get_contents($this->path, false, $this->streamContext);
            return $result;
        } catch (Throwable $th) {
            die('ERROR_EXECUTE_ANALYSIS: ' . $th->getMessage());
        }
    }
}