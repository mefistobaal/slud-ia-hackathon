<?php

/**
 * Global config for DB Conexion
 */
class Config
{
    const DB_NAME     = '';
    const DB_USERNAME = '';
    const DB_PASSWORD = '';
    const DB_HOST     = '';
    const DB_CHAR     = 'SET CHARSET utf8';
    const DB_OPT      = [PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION];//Atributos de ejecucion PDO
}
