<?php
/**
 * S.C.A AI Team
 *
 * API-Rest para analisis sentimental de tweets
 * @author Santiago Hurtado
 * @author Diego Pineda
 * @author Cristian Vazquez
 * @author Camilo Garcia
 */

require $_SERVER['DOCUMENT_ROOT'] . '/app/Kernel/Request.php';

use SCAAI\Kernel\Request;

class Index
{
    public function __construct(array $url)
    {
        try {
            $request = (empty($url['url']))
                ? new Request('')
                : new Request($url['url']);
            //Execute
            $request->execute();
        } catch (Throwable $th) {
            die('ERROR_CONSTRUCT_INDEX: ' . $th->getMessage());
        }
    }
}

//Start requests
new Index($_GET);
