-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 31-08-2019 a las 02:45:19
-- Versión del servidor: 10.4.7-MariaDB
-- Versión de PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dataslud`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sludpeople`
--

CREATE TABLE `sludpeople` (
  `tweet` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `valoracion humano` enum('positivo','negativo','neutral') COLLATE utf8mb4_unicode_ci NOT NULL,
  `puntaje` float NOT NULL,
  `valoracion cualitativa` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auto incremental` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
